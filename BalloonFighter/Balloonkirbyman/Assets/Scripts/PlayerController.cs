﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    public static PlayerController mainControl;

    // Put main variables here
    public int currentHealth, maxHealth,
                        defense,
                        currentEXP, expTNL,
                        abilityLV,
                        currentAmmo, maxAmmo;

    public int shieldOrb, sunTank;
    public int totalCompletionRate;
    public bool newStart;
    public bool newTank = false;
    public bool damageState = false;
    public bool isMoving = false;

    public int damageTaken;

    public int enemiesKilled;
    public float damageReceivedMultiplier;

    // Handle character movement speed here
    public float moveSpeed;
    public float characterDefaultSpeed;
    public float characterMaxSpeed;
    // Handle character acceleration over time here
    private float acceleration;
    public float accelerationMagnitude;

    // Handle character jumping height here
    public float jumpForceHeight;
    // Is check if the player is on the ground
    public bool isGrounded = false;
    public Transform groundCheck;
    private float groundRadius = 0.2F;
    public LayerMask whatIsGround;

    // Put enumerators for Unity inspector here
    public enum Difficulty {        Casual=1, Normal, Challenging, Insane     }
    public Difficulty difficultySetting;
    public enum SpecialAbility {    None, Weapon, Fire, Water, Air, Food          }
    public SpecialAbility specialAbility;
    public enum SubAbility {        None, Sword, Hammer, Flame, Explosion,
                                    Wind, Lightning, Water, Ice,
                                    Refrigerator, Freezer                   }
    public SubAbility subAbility;

    // Create class objects here
    private SetHealth setHealth = new SetHealth();
    private SetDefense defenseObj = new SetDefense();
    private SetDifficulty setDiff = new SetDifficulty();
    private Animator _playerAnimation;
    private Rigidbody2D _rbody2D;
    private SpriteRenderer _sprRend;

    // For testing purposes
    float tempTime;

    void Awake()
    {
        // Guarantees that there is only one PlayerController instance
        if (mainControl == null)
        {
            DontDestroyOnLoad(gameObject);
            mainControl = this;
        }
        else if (mainControl != this)
        {
            Destroy(gameObject);
        }
        // Initialize set difficulty object when difficulty is selected from previous scene
        setDiff.DifficultySetting((int)difficultySetting);
        enemiesKilled = setDiff.enemyCounter;
        damageReceivedMultiplier = setDiff.damageMultiplier;
    }

	void Start ()
    {
        // Use create component calls here to reduce memory usage
        _playerAnimation = mainControl.GetComponent<Animator>();
        _rbody2D = mainControl.GetComponent<Rigidbody2D>();
        _sprRend = mainControl.GetComponent<SpriteRenderer>();

	    if (newStart == true)
        {
            maxHealth = setHealth.SetMaxHealth(sunTank, newTank, newStart);
            currentHealth = setHealth.SetCurrentHealth(newTank, newStart, damageState);
        }
	}
	
	void Update ()
    {
        CheatingPrevention();
        Jumping();    
    }

    void FixedUpdate()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround);
        _playerAnimation.SetBool("grounded", isGrounded);

        _playerAnimation.SetFloat("vSpeed", _rbody2D.velocity.y);

        Movement();
        // Reinitialize Acceleration to prevent speeding off into nowhere
        if (Input.GetAxis("Horizontal") == 0)
            acceleration = 0;
    }

    public void Movement()
    {
        // Ensure acceleration magnitude is not 0 to prevent DIV/0 error
        if (accelerationMagnitude == 0)
            accelerationMagnitude = 1;

        // Re-render sprite animation to flip
        if (Input.GetAxis("Horizontal") < 0)
        {
            _sprRend.flipX = true;

            // Adjust acceleration if there is input
            if (Input.GetAxis("Horizontal") != 0)
            {
                acceleration -= moveSpeed / accelerationMagnitude;

                // Set a hard limit on the maximum acceleration the character will have when moving
                if (acceleration > characterMaxSpeed)
                    acceleration = characterMaxSpeed;
            }
            else
                acceleration = 0;
        }
        else if (Input.GetAxis("Horizontal") > 0)
        {
            _sprRend.flipX = false;

            // Adjust acceleration if there is input
            if (Input.GetAxis("Horizontal") != 0)
            {
                acceleration += moveSpeed / accelerationMagnitude;

                // Set a hard limit on the maximum acceleration the character will have when moving
                if (acceleration > characterMaxSpeed)
                    acceleration = characterMaxSpeed;
            }
            else
                acceleration = 0;
        }

        // Determine actual force applied to Rigidbody
        moveSpeed = Input.GetAxis("Horizontal");

        // Allows player to turn left and right without forcing movement speed back to 0--increases movement fluidity
        if (acceleration == 0 && moveSpeed > 0)
        {
            acceleration = 1;
            _rbody2D.velocity = new Vector2(moveSpeed * characterDefaultSpeed * acceleration, _rbody2D.velocity.y);
        }
        else if (acceleration == 0 && moveSpeed < 0)
        {
            acceleration = 1;
            _rbody2D.velocity = new Vector2(moveSpeed * characterDefaultSpeed * acceleration, _rbody2D.velocity.y);
        }
        else
        {
            _rbody2D.velocity = new Vector2(moveSpeed * characterDefaultSpeed * acceleration, _rbody2D.velocity.y);
        }
            
        // Play animation accordingly
        _playerAnimation.SetFloat("Speed", moveSpeed);
    }
    
    public void Jumping()
    {
        if (isGrounded && Input.GetAxis("Jump") != 0)
        {
            _playerAnimation.SetBool("grounded", false);
            _rbody2D.AddForce(new Vector2(0, jumpForceHeight));
        }
    }

    public void CheatingPrevention()
    {
        // FOR TESTING PURPOSES
        tempTime += Time.deltaTime;
        if (tempTime > 1)
        {
            tempTime = 0;

            maxHealth = setHealth.SetMaxHealth(sunTank, newTank, newStart);
            currentHealth = setHealth.SetCurrentHealth(damageState, damageTaken);

            defense = defenseObj.SetTotalDefense(shieldOrb);

            if (defense < 0)
            {
                defense = 0;
            }

            if (currentHealth > maxHealth)
            {
                currentHealth = maxHealth;
            }

            // Game over sequence
            if (currentHealth <= 0)
            {
                // Put in game over sequence
            }

        }
    }
}
