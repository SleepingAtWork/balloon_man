﻿using UnityEngine;
using System.Collections;

public class EnemyTypes
{
    public int HP, ATK; 
    public float typeDropBonus, rankDropBonus, expMultiplier;

    public EnemyTypes() { }

    public void SetEnemyTypes(int type, int rank)
    {
        SetEnemyStats(type, rank);
    }

    public void SetEnemyStats(int type, int rank)
    {
        // Normal
        if (type == 0 && rank == 0)
        {
            HP = 3;
            ATK = 2;
            typeDropBonus = 0.10F;
            rankDropBonus = 0.10F;
            expMultiplier = 1F;
        }
        // Normal Veteran
        else if (type == 0 && rank == 1)
        {
            HP = 5;
            ATK = 3;
            typeDropBonus = 0.10F;
            rankDropBonus = 0.20F;
            expMultiplier = 1.05F;
        }
        // Normal Elite
        else if (type == 0 && rank == 2)
        {
            HP = 10;
            ATK = 5;
            typeDropBonus = 0.10F;
            rankDropBonus = 0.30F;
            expMultiplier = 1.10F;
        }
        // Normal Champion
        else if (type == 0 && rank == 3)
        {
            HP = 25;
            ATK = 10;
            typeDropBonus = 0.10F;
            rankDropBonus = 0.40F;
            expMultiplier = 1.25F;
        }
        
        // Special
        else if (type == 1 && rank == 0)
        {
            HP = 10;
            ATK = 3;
            typeDropBonus = 0.20F;
            rankDropBonus = 0.10F;
            expMultiplier = 1.5F;
        }
        // Special Veteran
        else if (type == 1 && rank == 1)
        {
            HP = 20;
            ATK = 5;
            typeDropBonus = 0.20F;
            rankDropBonus = 0.20F;
            expMultiplier = 2F;
        }
        // Special Elite
        else if (type == 1 && rank == 2)
        {
            HP = 50;
            ATK = 8;
            typeDropBonus = 0.20F;
            rankDropBonus = 0.30F;
            expMultiplier = 2.5F;
        }
        // Special Champion
        else if (type == 1 && rank == 3)
        {
            HP = 75;
            ATK = 15;
            typeDropBonus = 0.20F;
            rankDropBonus = 0.40F;
            expMultiplier = 3F;
        }
    }
}
