﻿using UnityEngine;
using System.Collections;

public class SetHealth
{
    public int currentHealth, maxHealth,
                        sunTank;

    public const int BASE_HP = 10;
    public const int HP_PER_TANK = 3;

	public SetHealth()
    {    }

    public int SetMaxHealth(int collectedSunTanks, bool newTank, bool firstStart)
    {
        if (collectedSunTanks < 0)
        {
            collectedSunTanks = 0;
        }

        if (firstStart == true)
        {
            maxHealth = BASE_HP;
        }
        else if (firstStart == false)
        {
            if (newTank == true)
            {
                maxHealth = BASE_HP + (HP_PER_TANK * collectedSunTanks);
            }
            else if (newTank == false)
            {
                return maxHealth;
            }
        }
        
        return maxHealth;
    }
    public int SetCurrentHealth(bool newTank, bool firstStart, bool damageState)
    {
        if (firstStart == true && damageState == false)
        {
            currentHealth = maxHealth;
        }
        else
        {
            if (newTank == true)
            {
                currentHealth = maxHealth;
            }
        }
        return currentHealth;
    }

    public int SetCurrentHealth(bool damageState, int damageTaken)
    {
        if (damageState == true)
        {
            currentHealth -= damageTaken;
        }
        return currentHealth;
    }
}
