﻿using UnityEngine;
using System.Collections;

public class SetDefense
{
    int shieldOrb, defense;
    const int REQ_ORBS_FOR_DEF = 4;

    public SetDefense()
    { }

    public int SetTotalDefense(int sOrb)
    {
        shieldOrb = sOrb;

        defense = (int)Mathf.Floor(shieldOrb / REQ_ORBS_FOR_DEF);

        return defense;
    }

}
