﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour
{
    EnemyDropRate dropRateObj = new EnemyDropRate();
    EnemyTypes setType = new EnemyTypes();

    public int enemyHP, enemyATK;
    public float dropRate;
    public int expGiven, totalEXP;

    public enum EnemyType
    {
        Normal, Special
    }

    public enum EnemyRank
    {
        Normal, Veteran, Elite, Champion
    }

    public EnemyType enemyType;
    public EnemyRank enemyRank;

    void Awake()
    {
        setType.SetEnemyTypes((int)enemyType, (int)enemyRank);
        enemyHP = setType.HP;
        enemyATK = setType.ATK;

    }
    // Use this for initialization
    void Start ()
    {
        
    }
	
	// Update is called once per frame
	void Update ()
    {
        setType.SetEnemyTypes((int)enemyType, (int)enemyRank);
        dropRate = dropRateObj.CalculateDropRate(setType.typeDropBonus, setType.rankDropBonus);

        totalEXP = (int)(expGiven * setType.expMultiplier);
    }

}
