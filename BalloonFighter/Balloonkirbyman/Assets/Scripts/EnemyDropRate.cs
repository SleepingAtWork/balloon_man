﻿using UnityEngine;
using System.Collections;

public class EnemyDropRate
{
    public EnemyDropRate()
    { }

    public float CalculateDropRate(float typeDropRate, float rankDropRate)
    {
        float dropRate;
        float playerHPPercent;
        float difficultyDropRate;
        int difficulty;

        difficulty = (int)PlayerController.mainControl.difficultySetting;
        difficultyDropRate = 1F;

        playerHPPercent = (float)PlayerController.mainControl.currentHealth / (float)PlayerController.mainControl.maxHealth;
        dropRate = ((1.0F - playerHPPercent) / 1.0F) * (1.0F + typeDropRate + rankDropRate);

        if (difficulty == 0 || difficulty == 1)
            difficultyDropRate = 1F;
        else if (difficulty == 2)
            difficultyDropRate = 0.5F;
        else if (difficulty == 3)
            difficultyDropRate = 0.25F;

        dropRate = dropRate * difficultyDropRate;

        // Don't exceed 100%
        if (dropRate > 1F)
            dropRate = 1F;

        return dropRate;
    }
}
