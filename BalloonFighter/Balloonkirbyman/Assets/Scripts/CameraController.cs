﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
    public GameObject followObject;
    public Vector3 cameraOffset;


    void Awake()
    {
        QualitySettings.vSyncCount = 1;
        Application.targetFrameRate = 60;

#if UNITY_EDITOR
        QualitySettings.vSyncCount = 1;
        Application.targetFrameRate = 60;
#endif
    }

	// Use this for initialization
	void Start ()
    {
        cameraOffset = transform.position - followObject.transform.position;
	}
	
	// Update is called once per frame
	void Update ()
    {
        
	}

    void LateUpdate()
    {
        transform.position = followObject.transform.position + cameraOffset;       
    }
}
