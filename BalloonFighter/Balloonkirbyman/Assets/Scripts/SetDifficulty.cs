﻿using UnityEngine;
using System.Collections;

public class SetDifficulty
{
    public int difficulty, enemyCounter;
    public float damageMultiplier;

    const int DIFFICULTY_THRESHOLD = -20000000;
    const int DIFFICULTY_IMPOSSIBLE = 20000000;

    public SetDifficulty()
    {
        
    }

    public void DifficultySetting(int getDifficulty)
    {
        if (getDifficulty == 0 || getDifficulty == 1)
        {
            difficulty = 1;
            enemyCounter = DIFFICULTY_THRESHOLD;
            damageMultiplier = 1F;
        }
        else if (getDifficulty == 2)
        {
            difficulty = 2;
            enemyCounter = 0;
            damageMultiplier = 1F;
        }
        else if (getDifficulty == 3)
        {
            difficulty = 3;
            enemyCounter = 0;
            damageMultiplier = 2F;
        }
        else if (getDifficulty == 4)
        {
            difficulty = 4;
            enemyCounter = DIFFICULTY_IMPOSSIBLE;
            damageMultiplier = 2F;
        }
        
    }
}
